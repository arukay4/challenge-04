const http = require('http');
const { PORT = 8000 } = process.env;

const url = require('url')
const fs = require('fs');
const path = require('path');
const { parse } = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');


function onRequest(req, res) {
  let requestUrl = req.url;

    switch (requestUrl) {
        case "/":
            requestUrl = "index.html";
            break;
        case "/carimob":
            requestUrl = "carimobil.html";
            break;
        default:
            requestUrl = req.url;
            break;
      }

    const parseUrl = url.parse(requestUrl);
    const pathName = `${parseUrl.pathname}`;
    const extension = path.parse(pathName).ext;
    const absolutePath = path.join(PUBLIC_DIRECTORY, pathName);
    console.log(`extension`, extension);
    console.log(`absolute`, absolutePath);

    const mapContent = {
      ".css": "text/css",
      ".jpg": "image/jpg",
      ".svg": "image/svg+xml",
      ".html": "text/html",
      ".js": "text/javascript",
      ".app": "application/xml",
  }

  fs.exists(absolutePath, (exists) => {
    if (!exists) {
        res.writeHead(403);
        res.write("FILE NOT FOUND")
        return;
    }
  })
 
  fs.readFile(absolutePath, (err, data) => {
    if (err) {
      res.statusCode = 500;
      res.write("FILE NOT FOUND");
      console.log(err);
    } else {
      res.setHeader('Content-Type', mapContent[extension] || "text/plan");
      res.end(data)
    }
  });
}


const server = http.createServer(onRequest);

http.createServer((req,res) => {
  const reqUrl = req.url;
  const parseUrl = url.parse(req.url);
  const extension = path.parse(parseURL).ext
  console.log(parseURL, extension);
})

server.listen(PORT, '0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})
