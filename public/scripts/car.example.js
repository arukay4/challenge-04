class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="card">
    <div class="card-body">
      <img src="${this.image}" alt="" class="img-fluid" style="height:400px; object-fit:cover;">
      <p class="title-filter pt-3">${this.type} / ${this.model}</p>
      <p class="text-heading-two">IDR ${this.rentPerDay} / day</p>
      <p class="text-binar">${this.description}</p>
      <p class="text-binar"><i class="fa-solid fa-person"></i> ${this.capacity}</p>
      <p class="text-binar"><i class="fa-solid fa-gear"></i> ${this.transmission}</p>
      <p class="text-binar"><i class="fa-solid fa-calendar-days"></i> Tahun ${this.year}</p>
      <button class="btn-succes" style="width: 100%">Sewa Mobil ini!</button>
    </div>
  </div>
`;
  }
}
