class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.find_availableAt_date = document.getElementById("form_date").value;
    this.find_availableAt_time = document.getElementById("form_time").value;
    this.find_capacity = document.getElementById("form_capacity").value;

  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    this.clear();
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add("col-12", "col-md-6", "col-lg-4");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.find_capacity = document.getElementById("form_capacity").value;
    this.find_availableAt_date = document.getElementById("form_date").value;
    this.find_availableAt_time = document.getElementById("form_time").value;
    const datetime = new Date(`${this.find_availableAt_date} ${this.find_availableAt_time.substr(0, 4)}`)
    const beforeEpochTime = datetime.getTime();

    const cars = await Binar.listCars((item) => {
      const capacity_filter = this.find_capacity > 0 ? item.capacity == this.find_capacity : true;

      const itemDate = new Date(item.availableAt);
      const date_filter = itemDate.getTime() < beforeEpochTime;

      return capacity_filter && date_filter;
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
